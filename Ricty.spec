%global migu         migu-1m
%global migu_ver     20200307
%global migu_name    %{migu}-%{migu_ver}
%global migu_regular %{migu}-regular.ttf
%global migu_bold    %{migu}-bold.ttf
%global fontdir      %{_datarootdir}/fonts

Name:     Ricty
Version:  4.1.1
Release:  4%{?dist}
Summary:  True Type Font for programming in Linux.

Group:    User Interface/X 
License:  SIL Open Font License Version 1.1
URL:      http://komusubi.org
Source0:  Ricty-%{version}.tgz
Source1:  %{migu_name}.zip
Source2:  Inconsolata-Regular.ttf
Source3:  Inconsolata-Bold.ttf
Source4:  nerd-fonts.zip

BuildArch: noarch

BuildRequires: fontforge
BuildRequires: python
#Requires:

# memo 2017/06/18
# build Ricty-{version}.tgz manually
# mkdir Ricty-{version}
# mv ricty_generator.sh Ricty-{version}
# tar cvf - Ricty-{version} | gzip > Ricty-{version}.tgz
# 
#SRC_URL http://www.rs.tus.ac.jp/yyusa/ricty/ricty_generator.sh 
#SRC_URL https://github.com/google/fonts/raw/master/ofl/inconsolata/Inconsolata-Bold.ttf
#SRC_URL https://github.com/google/fonts/raw/master/ofl/inconsolata/Inconsolata-Regular.ttf
#SRC_URL http://iij.dl.sourceforge.jp/mix-mplus-ipa/63545/migu-1m-20150712.zip
%description

%prep
%{__rm} -rf %{migu_name}
%{__rm} -rf src font-patcher
%setup -q -b 1
%setup -q -b 4

%build
# exec -z option for "Disable visible zenkaku space"
sh ricty_generator.sh -z \
  ../../SOURCES/Inconsolata-Regular.ttf \
  ../../SOURCES/Inconsolata-Bold.ttf \
  ../%{migu_name}/%{migu_regular} \
  ../%{migu_name}/%{migu_bold}

for f in $(ls *.ttf); do
  ../font-patcher -out . -c ${f}
done

%install
%{__install} -dm 755 %{buildroot}%{fontdir}/%{name}
%{__install} -dm 755 %{buildroot}%{_defaultdocdir}/%{name}

%{__install} -m 644 %{name}*.ttf %{buildroot}%{fontdir}/%{name}/

%post
fc-cache -f %{fontdir}/%{name}

%files
%doc
%{fontdir}/%{name}
%{_defaultdocdir}/%{name}

%changelog
* Mon May 10 2021 komusubi <komusubi@gmail.com> 4.1.1-4
- add nerd fonts

* Sun May  9 2021 komsuubi <komusubi@gmail.com> 4.1.1-3
- update Inconsolata and migu-1m fonts

* Wed May  1 2019 komusubi <komusubi@gmail.com> 4.1.1-2
- fix package configuration

* Sat Apr  7 2018 komusubi <komusubi@gmail.com> 4.1.1-1
- upgrade Ricty 4.1.1

* Sat Jun 17 2017 komusubi <komusubi@gmail.com> 4.1.0-1
- upgradde Ricty 4.1.0

* Tue May 24 2016 komusubi <komusubi@gmail.com> 4.0.1-1
- upgrade Ricty 4.0.1 

* Wed Sep 23 2015 komusubi <komusubi@gmail.com> 3.2.4-2
- upgrade migu-1m 20150712

* Sun May  3 2015 komusubi <komusubi@gamil.com> 3.2.4-1
- update 3.2.4 

